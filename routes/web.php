<?php

use App\Http\Controllers\BarTabletopGameController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TabletopGameController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/gamelist',BarTabletopGameController::class);

Auth::routes();


Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::resource('roles', RoleController::class);

Route::resource('tabletopgames', TabletopGameController::class);

//// Routes for roles
////Index
//Route::get('/roles', [RoleController::class, 'index'])->name('roles.index');
//
////Create
//Route::get('/roles/create', [RoleController::class, 'create'])->name('roles.create');
////Show
//Route::get('/roles{role}', [RoleController::class, 'show'])->name('roles.show');
//
////Store
//Route::post('/roles/store', [RoleController::class, 'store'])->name('roles.store');
//
////Edit
//Route::get('/roles/{role}/edit', [RoleController::class, 'edit'])->name('roles.edit');
//
////Update
//Route::patch('/roles/{role}', [RoleController::class, 'update'])->name('roles.update');
//
////Hard Delete
//Route::delete('/roles/{role}', [RoleController::class, 'destroy'])->name('roles.destroy');
