@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form action="{{route('roles.update', $role->id)}}" method="post">
                    @csrf
                    @method('patch')
                    <label for="title">Title</label>
                    <input type="text" name="title" id="title" value="{{$role->title}}">
                    <button type="submit">Edit role</button>
                </form>
            </div>
        </div>
    </div>
@endsection
