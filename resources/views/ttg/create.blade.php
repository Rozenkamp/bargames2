@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form action="{{route('roles.store')}}" method="post">
                    @csrf
                    <label for="title">Title</label>
                    <input type="text" name="title" id="title">
                    <button type="submit">Add role</button>
                </form>
            </div>
        </div>
    </div>
@endsection
