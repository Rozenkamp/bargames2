@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @foreach($bartabletopgames as $bartabletopgame)
                        <div class="card-header">
                           <a href="{{ route('gamelist.show', $bartabletopgame->id) }}">
                            <strong>{{ $bartabletopgame->title }}</strong></a>
                        </div>
                        <div class="card-body">  </div>
                        <div class="card-footer">
                        <a href="{{ route('gamelist.edit', $bartabletopgame->id) }}">Update</a>
                            <form action="{{ route('gamelist.destroy', $bartabletopgame->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit">Delete Game</button>
                            </form>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
