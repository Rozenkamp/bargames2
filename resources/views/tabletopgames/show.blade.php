@extends('tabletopgames.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Tabletop Game</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('tabletopgames.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Title:</strong>
                {{ $tabletopgame->title }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Image:</strong>
                <img src="/images/{{ $tabletopgame->image }}" width="500px">
            </div>
        </div>
    </div>
@endsection
