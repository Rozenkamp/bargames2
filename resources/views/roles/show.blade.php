@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <strong>{{ $role->title }}</strong><br>
                    </div>
                    <div class="card-body">
                        @foreach($role->userRole as $user)
                            {{ $user->name }} <br>
                        @endforeach

                    </div>
                    <div class="card-footer">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
