@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @foreach($roles as $role)
                        <div class="card-header">
                           <a href="{{ route('roles.show', $role->id) }}">
                            <strong>{{ $role->title }}</strong></a>
                        </div>
                        <div class="card-body">  </div>
                        <div class="card-footer">
                        <a href="{{ route('roles.edit', $role->id) }}">Update</a>
                            <form action="{{ route('roles.destroy', $role->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit">Delete Role</button>
                            </form>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
