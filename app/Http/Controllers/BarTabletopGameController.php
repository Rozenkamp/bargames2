<?php

namespace App\Http\Controllers;

use App\Models\Bar_tabletop_game;
use App\Http\Requests\StoreBar_tabletop_gameRequest;
use App\Http\Requests\UpdateBar_tabletop_gameRequest;


class BarTabletopGameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bttg = Bar_tabletop_game::all();

        return view('gamelist.index', 'bartabletopgame');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBar_tabletop_gameRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBar_tabletop_gameRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bar_tabletop_game  $bar_tabletop_game
     * @return \Illuminate\Http\Response
     */
    public function show(Bar_tabletop_game $bar_tabletop_game)
    {
        $bttg = Bar_tabletop_game::find('id')->get();
        $uri = 'https://boardgamegeek.com/xmlapi/boardgame/';
        $xmlObject = simplexml_load_file($uri);
        $json = json_encode($xmlObject);
        $bartabletopgame = json_decode($json, true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bar_tabletop_game  $bar_tabletop_game
     * @return \Illuminate\Http\Response
     */
    public function edit(Bar_tabletop_game $bar_tabletop_game)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBar_tabletop_gameRequest  $request
     * @param  \App\Models\Bar_tabletop_game  $bar_tabletop_game
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBar_tabletop_gameRequest $request, Bar_tabletop_game $bar_tabletop_game)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bar_tabletop_game  $bar_tabletop_game
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bar_tabletop_game $bar_tabletop_game)
    {
        //
    }
}
