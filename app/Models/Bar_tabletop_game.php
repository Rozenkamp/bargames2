<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bar_tabletop_game extends Model
{
    use HasFactory;

    protected $fillable = [
        'bgg_id',
        'title',
        'yearpublished',
        'image',
        'thumbnail',
        'min_players',
        'max_players',
        'playingtime'
        ];

}
