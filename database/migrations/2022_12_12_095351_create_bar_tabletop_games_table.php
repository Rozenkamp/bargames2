<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bar_tabletop_games', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('bgg_id');
            $table->string('title');
            $table->integer('yearpublished');
            $table->text('image');
            $table->text('thumbnail');
            $table->integer('min_players');
            $table->integer('max_players');
            $table->integer('playingtime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bar_tabletop_games');
    }
};
