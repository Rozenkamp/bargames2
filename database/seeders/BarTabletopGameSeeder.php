<?php

namespace Database\Seeders;

use App\Models\Bar_tabletop_game;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BarTabletopGameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $xmlString = file_get_contents(public_path('windesheim999.xml'));
        $xmlObject = simplexml_load_string($xmlString);
        $json = json_encode($xmlObject);
        $phpArray = json_decode($json, true);

        foreach ($phpArray['item'] as $bg) {
            Bar_tabletop_game::insert([
                'bgg_id' => $bg['@attributes']['objectid'],
                'title' => $bg['name'],
                'yearpublished' => $bg['yearpublished'],
                'image' => $bg['image'],
                'thumbnail' => $bg['thumbnail'],
                'min_players' => 2,//$bg['stats']['@attributes']['minplayers'],
                'max_players' => 4,//$bg['stats']['@attributes']['maxplayers'],
                'playingtime' => 6,//$bg['stats']['@attributes']['playingtime']
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
