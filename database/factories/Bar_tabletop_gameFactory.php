<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Bar_tabletop_game>
 */
class Bar_tabletop_gameFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $xmlString = file_get_contents(public_path('windesheim999.xml'));
        $xmlObject = simplexml_load_string($xmlString);
        $json = json_encode($xmlObject);
        $phpArray = json_decode($json, true);

        foreach ($phpArray['item'] as $bg) {
            return [
                'bgg_id' => $bg['@attributes']['objectid'],
                'title' => $bg['name'],
                'yearpublished' => $bg['yearpublished'],
                'image' => $bg['image'],
                'thumbnail' => $bg['thumbnail'],
                'min_players' => $bg['stats']['@attributes']['minplayers'],
                'max_players' => $bg['stats']['@attributes']['maxplayers'],
                'playingtime' => $bg['stats']['@attributes']['playingtime']
            ];
        }
    }
}
